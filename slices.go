package main

import (
	"fmt"
	"strconv"
)

func main() {

	s := make([]string, 3)

	for i := 0; i < 10; i++ {
		s = append(s, strconv.Itoa(i))

	}

	s = append(s, "hello", "world", "keep", "adding")

	fmt.Println(s)

	// try deleting
	s = s[:len(s)-1]

	fmt.Println(s)

	c := make([]string, len(s))
	copy(c, s)
	fmt.Println(len(c))

	//2d array
	twoD := make([][]int, 3)
	for i := 0; i < 3; i++ {
		innerLen := i + 1
		twoD[i] = make([]int, innerLen)
		for j := 0; j < innerLen; j++ {
			twoD[i][j] = i + j
		}
	}
	fmt.Println("2d: ", twoD)

}
