package main

import "fmt"

func main() {

	num := 9

	if num < 9 {
		fmt.Println("The number is smaller than 9")
	} else if num > 9 {
		fmt.Println("The number is larger than 9")
	} else {
		fmt.Println("The num is equal to 9")
	}

}
