package main

import "fmt"

func main() {

	t := 4

	switch t {
	case 1:
		fmt.Println("It's 1")
	case 2:
		fmt.Println("It's 2")
	case 3:
		fmt.Println("It's 3")
	}

}
