package main

import "fmt"

func main() {

	var a = [5]int{1, 2, 3, 4, 5}

	fmt.Println(a[:1]) // array support slice operator

	b := [4][3]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}}

	for i := 0; i < len(b); i++ {
		for j := 0; j < len(b[0]); j++ {
			fmt.Println(b[i][j])
		}
	}

}
