package main

import "fmt"

func main() {

	m := make(map[string]int)

	m["apple"] = 1
	m["orange"] = 2
	m["peach"] = 10

	fmt.Println(m)

	// Cannot use copy!
	// mCopy := make(map[string]int)
	// copy(mCopy, m)

	fmt.Println(m["peach"])

	//The optional second return value when getting a value from a map indicates if the key was present in the map.
	// This can be used to disambiguate between missing keys and keys with zero values like 0 or "".
	a, bool := m["test"]
	fmt.Println(a)
	fmt.Println(bool)

	a_, bool_ := m["orange"]
	fmt.Println(a_)
	fmt.Println(bool_)

	n := map[string]int{"foo": 1, "bar": 2}
	fmt.Println("map:", n)

}
