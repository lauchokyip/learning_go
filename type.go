package main

import "fmt"

func main() {

	const myStringHello = "initial" // const does not have type
	var hello = "hello"

	//    An untyped constant has a default type which is the type to which the constant is implicitly converted in contexts where a typed value is required.
	fmt.Printf("%T: %v\n", myStringHello, myStringHello)
	fmt.Printf("%T: %v\n", hello, hello)

}
